RT-clock based virtual-machine detection
========================================

This code tries to determine if this computer is a real device or virtual
machine (running in software like Xen, Qemu or VirtualBox). The idea is to
lock processor core and check real-time clock constantly, measuring how
irregular timestampls were got. See project wiki for more information.
