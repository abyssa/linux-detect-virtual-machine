#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/hpet.h>
MODULE_LICENSE("GPL");
#define MSG_PREFIX "rtvmdetect: "
#define counter() ((signed)hpet_readl_(HPET_COUNTER))

int rtvmdetect_init(void)
{
    int (*is_hpet_enabled_)(void);
    unsigned (*hpet_readl_)(unsigned);
    int sumdeltas;
    int sumsquares;
    int delta;
    int curr;
    int prev;
    int num;
    int variance;

    is_hpet_enabled_ = (void*)kallsyms_lookup_name("is_hpet_enabled");
    hpet_readl_      = (void*)kallsyms_lookup_name("hpet_readl");

    if (!is_hpet_enabled_ || !hpet_readl_ ) {
        printk(KERN_ERR MSG_PREFIX "unable to lookup hpet_* symbols\n");
        return -1;
    }

    if (!is_hpet_enabled_()) {
        printk(KERN_ERR MSG_PREFIX "hpet is disabled, exitting\n");
        return -1;
    }

    num = 0;
    prev = counter();
    do {
        curr  = counter();
        delta = curr - prev;
        prev  = curr;
        ++num;
    } while (!(prev >= 0 && curr < 0));

    printk(KERN_INFO MSG_PREFIX "counter flipped sign in %d ticks\n", num);
    return -1;


    sumdeltas  = 0;
    sumsquares = 0;
    num = 0;
    prev = counter();
    while (true) {
        delta = counter() - prev;
        prev += delta;
        if (delta < 0)
            break;

        sumdeltas  += delta;
        if (sumdeltas < 0) {
            sumdeltas -= delta;
            break;
        }

        sumsquares += delta*delta;
        if (sumsquares < 0) {
            sumsquares -= delta*delta;
            break;
        }

        ++num;
    }

    variance = (sumsquares - sumdeltas*sumdeltas / num) / num;

    printk(KERN_INFO MSG_PREFIX "num = %d, variance = %d\n", num, variance);
    return 0;
}

void rtvmdetect_exit(void)
{
    printk(KERN_INFO MSG_PREFIX "exitting\n");
}

module_init(rtvmdetect_init);
module_exit(rtvmdetect_exit);
